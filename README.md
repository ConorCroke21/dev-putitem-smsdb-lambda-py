# dev-putitem-smsdb-lambda-py

This lambda service is written in python. Its purpose is to take a request and insert it into the DynamoDB table OutboundSms for recording what SMS have been sent.

![outboundEmail](https://dev-webmvc-dotnet20210305091410.azurewebsites.net/Images/outboundsms.PNG){:height="50%" width="50%"}

