import json
import boto3
import uuid
import datetime

def lambda_handler(event, context):

    date = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
    
    #dynamodb variables
    dynamodb = boto3.resource('dynamodb')
    table = dynamodb.Table('OutboundSms')

    #generate uuid
    smsId = uuid.uuid4().hex
    
    try:
        #request variables
        to = event['Request']['SMS']['to']
        body = event['Request']['SMS']['body']
    
        table.put_item(
            Item={
                'smsId':smsId,
                'datetime':date,
                'to':to,
                'body': body
            }
        )
    except:
        return {
            "statusCode": 500,
            "body": json.dumps(
                {
                    "message": "Failure"
                }
            ),
        }
    else:
        return {
            "statusCode": 200,
            "body": json.dumps(
                {
                    "message": "Success"
                }
            ),
        }